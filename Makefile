all: build

dep: ## fetch all dependencies
	@go get -v -t ./...

tools: dep ## Fetch useful tools for development
	@go get -u golang.org/x/lint/golint

lint: tools ## Lint the files
	@${GOPATH}/bin/golint -set_exit_status ./...

build: dep ## Build the binary
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v

test: dep ## Run all tests
	@go test ./... -v

test-cover: dep ## Run tests with code coverage
	@go test -cover ./...

test-race: dep ## Run tests with race detector
	@go test -race ./...

run: dep ## Directly run the code
	@go run *.go
