############################
# STEP 1 build executable binary
############################
FROM golang:1.12 AS builder

WORKDIR $GOPATH/src/test-picture-provider

RUN apt-get install git

COPY . .

RUN make build

############################
# STEP 2 build a small image
############################
FROM alpine

WORKDIR /app

RUN apk add --no-cache ca-certificates
COPY --from=builder /go/src/test-picture-provider/test-picture-provider /app/
RUN mkdir -p /app/pictures

EXPOSE 80

ENTRYPOINT ["./test-picture-provider"]
