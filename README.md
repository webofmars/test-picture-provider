# Test Picture Provider
This repo contains an application for providing test images in a local env.

## Usage
### With docker
The easiest way is to use docker

```
docker run -p 1234:80 bosix/test-picture-provider
```

or docker-compose

```yaml
version: "3"
services:
    test-picture-provider:
        image: bosix/test-picture-provider
        ports:
        - 1234:80
```
```
docker-compose up -d
```

to run this app.

### With golang
If you don't like docker or just want to use golang to run this app use the following commands:
```
git checkout https://gitlab.com/Bosi/test-picture-provider.git
cd test-picture-provider
go run main.go
```

## Configuration
There are several options you can set via env var if you like:

| name | default value | description | 
| --- | --- | --- |
| MAX_NUMBER_OF_PICTURES | 100 | number of images which will be downloaded to cache |
| MAX_PICTURE_SOURCE_URL | https://lorempixel.com/600/480/ | url the images will be downloaded from |

Example: 
```yaml
version: "3"
services:
    test-picture-provider:
        image: bosix/test-picture-provider
        ports:
        - 1234:80
        environment:
        - "MAX_NUMBER_OF_PICTURES=50"
        - "MAX_PICTURE_SOURCE_URL=https://lorempixel.com/1000/500/cats/"
```
```
docker-compose up -d
```

## Persistent pictures
Per default the container will download new images each time it is started. You can simply mount a volume to prevent this:

```yaml
version: "3"
services:
    test-picture-provider:
        image: bosix/test-picture-provider
        ports:
        - 1234:80
        volumes:
        - ./pictures:/app/pictures 
```
```
docker-compose up -d
```